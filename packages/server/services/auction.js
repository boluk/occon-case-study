const Redis = require('./redis');
const logger = require('./logger');

const tickInterval = process.env.TICK_INTERVAL || 1000;
const auctionDuration = process.env.AUCTION_DURATION || 60000;

function initializeAuction(socket) {
  const initTime = Date.now();
  const deadline = initTime + auctionDuration;
  logger.trace({ socket: socket.id, type: 'connection:init' });
  Redis.set(`${socket.id}:init`, initTime);
  Redis.set(`${socket.id}:deadline`, deadline);
  const ticker = setInterval(() => {
    socket.emit('ticker', deadline - Date.now());
  }, tickInterval);
  setTimeout(() => {
    socket.removeAllListeners();
    logger.debug({ socket: socket.id, type: 'cleanup' });
    pushResults(socket);
    clearInterval(ticker);
  }, auctionDuration);
  socket.emit('deadline', deadline);
  socket.on('refresh', () => {
    logger.trace({ socket: socket.id, type: 'refresh' });
    pushHighest(socket);
    if (Date.now() > deadline) {
      socket.emit('ticker', 0);
      pushResults(socket);
    } else {
      socket.emit('ticker', deadline - Date.now());
    }
  });
  socket.on('disconnect', () => {
    logger.trace({ socket: socket.id, type: 'connection:end' });
  });
}

function registerUser(socket, userName) {
  socket.on(userName, function(bid) {
    setLatest(socket, userName, bid);
    pushHighest(socket);
  });
}

function pushHighest(socket) {
  Redis.zrevrange([socket.id, 0, 0, 'WITHSCORES'], (err, keys) => {
    if (err) {
      logger.error({
        socket: socket.id,
        type: 'highest',
        err
      });
    } else {
      const [highestBidder, highestBid] = keys;
      logger.trace({ socket: socket.id, type: 'highest' });
      socket.emit('highest', `${highestBidder} leads with ${highestBid}`);
    }
  });
}

function setLatest(socket, user, value) {
  Redis.zadd([socket.id, value, user], (err) => {
    if (err) {
      logger.error({
        socket: socket.id,
        type: 'bid',
        user,
        err
      });
    } else {
      socket.emit(`${user}Latest`, value);
    }
  });
}

function pushResults(socket) {
  Redis.zrevrange([socket.id, 0, -1, 'WITHSCORES'], (err, keys) => {
    if (err) {
      logger.error({
        socket: socket.id,
        type: 'result:range',
        err
      });
    } else {
      const [winner,winnersBid,,winningBid] = keys;
      if (winner) {
        socket.emit('result', `${winner} won for ${winningBid || winnersBid}`);
      } else {
        socket.emit('result', 'no participants :(');
      }
    }
  })
}

module.exports = {
  initializeAuction,
  registerUser,
}