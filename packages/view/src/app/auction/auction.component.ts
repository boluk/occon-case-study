import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuctionService } from '../auction.service';


@Component({
  selector: 'app-auction',
  templateUrl: './auction.component.html',
  styleUrls: ['./auction.component.css'],
})
export class AuctionComponent implements OnInit, OnDestroy {

  connection;
  user1;
  user2;
  user3;
  user1Latest = 0;
  user2Latest = 0;
  user3Latest = 0;
  deadline;
  result;
  highest;
  ticker;

  constructor(private auctionService: AuctionService) { }

  bid(user) {
    this.auctionService.bid(user, this[user]);
    this[user] = '';
  }

  refresh() {
    this.auctionService.refresh();
  }

  ngOnInit() {
    this.connection = this.auctionService.initialize().subscribe((message: any[]) => {
      const [key, value] = message;
      this[key] = value;
      if (key === 'result') {
        this.ticker = 0;
      }
    });
  }

  ngOnDestroy() {
    this.connection.unsubscribe();
  }

}
