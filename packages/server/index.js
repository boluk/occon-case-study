const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http, { serveClient: false });
const { initializeAuction, registerUser } = require('./services/auction');

const port = process.env.SERVER_PORT || 3000;

app.get('/', function(_, res){
  res.sendFile(__dirname + '/assets/index.html');
});

app.use(
  '/',
  express.static('assets/', {
    maxAge: '3d',
    setHeaders: res => {
      res.set({
        'Access-Control-Allow-Origin': '*',
      });
    },
  }),
);

io.on('connection', function(socket){
  initializeAuction(socket);
  socket.on('register user', function(userName) {
    registerUser(socket, userName);
  });
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(port, function(){
  console.log(`listening on *:${port}`);
});