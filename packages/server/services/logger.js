const pino = require('pino')();

const pinoApp = pino.child({
  appname: 'auctioneer',
  level: process.env.LOG_LEVEL || 'info',
});

module.exports = pinoApp;