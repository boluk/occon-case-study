export const environment = {
  production: true,
  auctionHost: 'localhost',
  auctionPort: 3000,
};
