import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuctionService {

  private url = `http://${environment.auctionHost}:${environment.auctionPort}`;
  private socket;

  bid(user, amount) {
    this.socket.emit(user, amount);
  }

  refresh() {
    this.socket.emit('refresh');
  }

  initialize() {
    const observable = new Observable(observer => {
      this.socket.on('deadline', (data) => {
        observer.next(['deadline', data]);
      });
      this.socket.on('highest', (data) => {
        observer.next(['highest', data]);
      });
      this.socket.on('result', (data) => {
        observer.next(['result', data]);
      });
      this.socket.on('ticker', (data) => {
        observer.next(['ticker', data]);
      });
      this.socket.on('ticker', (data) => {
        observer.next(['ticker', data]);
      });
      this.socket.on('user1Latest', (data) => {
        observer.next(['user1Latest', data]);
      });
      this.socket.on('user2Latest', (data) => {
        observer.next(['user2Latest', data]);
      });
      this.socket.on('user3Latest', (data) => {
        observer.next(['user3Latest', data]);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  constructor() {
    this.socket = io(this.url);
    this.socket.emit('register user', 'user1');
    this.socket.emit('register user', 'user2');
    this.socket.emit('register user', 'user3');
  }
}
