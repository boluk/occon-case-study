const redis = require('redis');
const logger = require('./logger');
const host = process.env.REDIS_HOST || 'localhost';
const port = process.env.REDIS_PORT || 6379;

const Redis = redis.createClient({
  host,
  port,
  retry_strategy: ({ error, attempt }) => {
    if (error) {
      logger.error(error);
    }
    logger.info(`redis retry attempt ${attempt}`);
    if (attempt > 5) {
      logger.error('giving up :( redis retry count > 5');
      process.exit(1);
      return;
    }
    return 3000;
  },
});

Redis.select(1, (err) => {
  if (err) {
    logger.error('could not select db on init');
  }

  logger.debug('attaching events to redis client');

  Redis.on('ready', () => {
    Redis.select(1);
    logger.debug('selected db 1');
  })

  Redis.on('error', err => {
    logger.error(`redis error: ${err}`);
  });
  Redis.on('end', () => {
    logger.debug('end notice on redis');
  });
});

module.exports = Redis;
