# OCCON Case Study

This is a simple second-price auction app based on the specs provided by OCCON for their hiring process.

View and Server packages are separated for clarity and to reduce complexity. Yarn workspaces are utilized to manage the packages.

## Install & run

* The project was developed on node v11.6.0. It hasn't been tested on earlier versions.

* Install @angular/cli globally if you haven't.
```
npm install -g @angular/cli
```
* Install dependencies through yarn (in the root directory)
```
yarn
```
* start script defined in project root builds and serves the app
```
yarn start
```


## Build

The view package requires a build step and it is handled by @angular/cli. The output is placed under the `/assets` folder in server package.

Running `yarn build` in the root directory will achieve a production build. For a development setup running `ng serve` in the view package directory will suffice.

Running `yarn serve` in the root directory will start the server.

## Environment Variables

`TICK_INTERVAL` determines the frequency of the timer updates pushed to the clients, in ms. Defaults to `1000`.

`AUCTION_DURATION` determines the duration of a single auction, in ms. Defaults to `60000`.

`LOG_LEVEL` determines the minimum level of logs pino will output, default is INFO.

`SERVER_PORT` determines which port the server will listen to. Defaults to `3000`.

`REDIS_HOST` specifies the hostname of the redis instance the app will use. Defaults to `localhost`.

`REDIS_PORT` specifies the port of the redis instance the app will use. Defaults to `6379`.

## Project Structure

I relied on `express.js` for its familiarity and ease of use for the main server. A separate `http` instance is created for `socket.io`.

I used a custom logger, `pino`, out of habit. It makes defining log levels and piping the logs to a remote server trivial through its many adapters (I'm quite fond of `pino-elasticsearch`).

Most of the sorting logic is offloaded to the redis instance the app will use (through the sorted set data structure). It is a core dependency and the process will exit after 5 failed connection attempts.

I organised the logic in 'services' in both the angular app and the node.js server. Auction services in both packages handle connections (to each other and to redis). Redis service exports a single client (we are using a single db).
